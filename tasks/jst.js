/*
 * grunt-contrib-jst-ext
 * https://gitlab.com/3nvy/grunt-contrib-jst-ext
 *
 * Licensed under the MIT license.
 */

/*
 * Based on:
 * grunt-contrib-jst
 * http://gruntjs.com/
 *
 * Copyright (c) 2016 Tim Branyen, contributors
 * Licensed under the MIT license.
 */

'use strict';

var _ = require('underscore');
var lo = require('lodash');
var chalk = require('chalk');
var cheerio = require('cheerio');

module.exports = function (grunt) {
    // filename conversion for templates
    var defaultProcessName = function (name) { return name; };

    grunt.registerMultiTask('jst-ext', 'Compile merged underscore templates to pre-compiled JST file', function () {
        var lf = grunt.util.linefeed;
        var lib = require('./lib/jst');
        var options = this.options({
            namespace: 'JST',
            templateSettings: {},
            useLodashTemplates: true,
            preventTemplateOverwrite: true,
            sortingFn: (a, b) => a - b,
            processContent: function (src) {
                var $ = cheerio.load("<div>" + src + "</div>");
                return $('div').children().toArray();
            },
            separator: lf + lf
        });

        // assign filename transformation functions
        var processName = options.processName || defaultProcessName;

        var nsInfo;
        if (options.namespace !== false) {
            nsInfo = lib.getNamespaceDeclaration(options.namespace);
        }

        var knowndTemplates = [];

        this.files.forEach(function (f) {
            var output = f.src.filter(function (filepath) {
                // Warn on and remove invalid source files (if nonull was set).
                if (!grunt.file.exists(filepath)) {
                    grunt.log.warn('Source file ' + chalk.cyan(filepath) + ' not found.');
                    grunt.log.warn('Source file ' + chalk.cyan(filepath) + ' not found.');
                    return false;
                } else {
                    return true;
                }
            })
                .sort(options.sortingFn)
                .map(function (filepath) {
                    return options.processContent(grunt.file.read(filepath))
                        .filter(function (el) {
                            if (options.preventTemplateOverwrite) return true;
                            var filename = el.attribs.id ? "#" + el.attribs.id : "." + el.attribs.class;
                            if (knowndTemplates.includes(filename)) return false;
                            knowndTemplates.push(filename);
                            return true;
                        })
                        .map(function (el) {
                            var compiledTemplate = options.useLodashTemplates
                                ? lo.template(el.children.length === 0 ? '' : el.children[0].data, false, options.templateSettings).source
                                : _.template(el.children.length === 0 ? '' : el.children[0].data, false, options.templateSettings).source;

                            var filename = el.attribs.id ? "#" + el.attribs.id : "." + el.attribs.class;

                            if (options.prettify) compiledTemplate = compiledTemplate.replace(/\n/g, '');
                            if (options.amd && options.namespace === false) return 'return ' + compiledTemplate;

                            return nsInfo.namespace + '[' + JSON.stringify(filename) + '] = ' + compiledTemplate + ';';
                        })
                        .join(grunt.util.normalizelf(options.separator))
                })

            if (output.length < 1) {
                grunt.log.warn('Destination not written because compiled files were empty.');
            } else {
                if (options.namespace !== false) {
                    output.unshift(nsInfo.declaration);
                }
                if (options.amd) {
                    if (options.prettify) {
                        output.forEach(function (line, index) {
                            output[index] = '  ' + line;
                        });
                    }
                    output.unshift('define(function(){');
                    if (options.namespace !== false) {
                        // Namespace has not been explicitly set to false; the AMD
                        // wrapper will return the object containing the template.
                        output.push('  return ' + nsInfo.namespace + ';');
                    }
                    output.push('});');
                }
                grunt.file.write(f.dest, output.join(grunt.util.normalizelf(options.separator)));
                grunt.log.writeln('File ' + chalk.cyan(f.dest) + ' created.');
            }
        });

    });
};
